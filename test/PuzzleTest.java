import static org.junit.Assert.*;
import org.junit.Test;

/** Testklass.
 */
public class PuzzleTest {

   @Test (timeout=200000)
   public void test1solution() {
      Puzzle puzzle = new Puzzle(new String[]{"SEND", "MORE", "MONEY"});
      assertEquals ("Incorrect number of results", 1, puzzle.getResultCount());
      assertEquals ("Incorrect results", "[[9567, 1085, 10652]]", puzzle.getResults().toString());
   }

   @Test (timeout=200000)
   public void test2solutions() {
      Puzzle puzzle = new Puzzle(new String[]{"ABCDEFGHIJAB", "ABCDEFGHIJA", "ACEHJBDFGIAC"});
      System.out.println(puzzle.getResults());
      assertEquals ("Incorrect number of results", 2, puzzle.getResultCount());
      assertEquals ("Incorrect results",
              "[[123456789012, 12345678901, 135802467913], [235184679023, 23518467902, 258703146925]]",
              puzzle.getResults().toString());
   }

   @Test (timeout=200000)
   public void test234solutions() {
      Puzzle puzzle = new Puzzle(new String[]{"YKS", "KAKS", "KOLM"});
      assertEquals ("Incorrect number of results", 234, puzzle.getResultCount());
   }

   @Test (timeout=200000)
   public void testNoSolutions() {
      Puzzle puzzle = new Puzzle(new String[]{"CBEHEIDGEI", "CBEHEIDGEI", "BBBBBBBBBB"});
      assertEquals ("Incorrect number of results", 0, puzzle.getResultCount());
      assertEquals ("Incorrect results", "[]", puzzle.getResults().toString());
   }

   @Test (expected=NullPointerException.class)
   public void testNullException() {
      Puzzle puzzle = new Puzzle(new String[]{"C", null, "C"});
   }

   @Test (expected=NullPointerException.class)
   public void testEmptyException() {
      Puzzle puzzle = new Puzzle(new String[]{"C", null, "C"});
   }
}

