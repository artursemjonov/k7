import java.util.*;
import java.util.stream.Collectors;

/**
 * Partial solution from https://www.pepcoding.com/resources/data-structures-and-algorithms-in-java-levelup/recursion-and-backtracking/cryptarithmetic-official/ojquestion.
 */

public class Puzzle {

   private String[] words;
   private HashMap<Character, Integer> charIntMap = new HashMap<>();
   private StringBuilder unique = new StringBuilder();
   private  boolean[] usedNumbers = new boolean[10];
   private Set<Character> cantBeZero;

   private List<List<Long>> results = new ArrayList<>();
   private int resultCount = 0;

   public Puzzle(String[] words) {
      for (int index = 0; index < words.length; index++) {
         String word = words[index];

         if (word == null) {
            throw new NullPointerException(
                    String.format("Can not solve puzzle. Word at index <%s> was null in input <%s>",
                            index, Arrays.toString(words)));
         }

         if (word.isEmpty()) {
            throw new IllegalArgumentException(
                    String.format("Can not solve puzzle. Word at index <%s> was empty in input <%s>",
                            index, Arrays.toString(words)));
         }
      }

      this.words = words;

      extractData();
      solve(0);
   }

   public static void main (String[] args) {
      String[] input1 = new String[]{"SEND", "MORE", "MONEY"};
      Puzzle puzzle1 = new Puzzle(input1);
      System.out.println("Input: " + Arrays.toString(input1));
      System.out.println("Number of results: " + puzzle1.resultCount);
      System.out.println("Results: " + puzzle1.results);
      System.out.println();

      String[] input2 = new String[]{"YKS", "KAKS", "KOLM"};
      Puzzle puzzle2 = new Puzzle(input2);
      System.out.println("Input: " + Arrays.toString(input2));
      System.out.println("Number of results: " + puzzle2.resultCount);
      System.out.println("First result: " + puzzle2.results.get(0));
      System.out.println();

      String[] input3 = new String[]{"ABCDEFGHIJAB", "ABCDEFGHIJA", "ACEHJBDFGIAC"};
      Puzzle puzzle3 = new Puzzle(input3);
      System.out.println("Input: " + Arrays.toString(input3));
      System.out.println("Number of results: " + puzzle3.resultCount);
      System.out.println("Results: " + puzzle3.results);
      System.out.println();

      String[] input7 = new String[]{"CD", "D", "A"};
      Puzzle puzzle7 = new Puzzle(input7);
      System.out.println("Input: " + Arrays.toString(input7));
      System.out.println("Number of results: " + puzzle7.resultCount);
      System.out.println("Results: " + puzzle7.results);
      System.out.println();

      String[] input8 = new String[]{"AAA", "AAA", "DBAAG"};
      Puzzle puzzle8 = new Puzzle(input8);
      System.out.println("Input: " + Arrays.toString(input8));
      System.out.println("Number of results: " + puzzle8.resultCount);
      System.out.println("Results: " + puzzle8.results);
      System.out.println();

      String[] input9 = new String[]{"AA", "AA", "CBD"};
      Puzzle puzzle9 = new Puzzle(input9);
      System.out.println("Input: " + Arrays.toString(input9));
      System.out.println("Number of results: " + puzzle9.resultCount);
      System.out.println("Results: " + puzzle9.results);
      System.out.println();

      String[] input10 = new String[]{"ABCDEFGHIJK", "ABCDEFGHIJA", "ACEHJBDFGIAC"};
      Puzzle puzzle10 = new Puzzle(input10);
      System.out.println("Input: " + Arrays.toString(input10));
      System.out.println("Number of results: " + puzzle10.resultCount);
      System.out.println("Results: " + puzzle10.results);
      System.out.println();

      String[] input4 = new String[]{"C", null, "C"};
      System.out.println("Input: " + Arrays.toString(input4));
      Puzzle puzzle4 = new Puzzle(input4);
      System.out.println();

      String[] input5 = new String[]{"C", "", "A"};
      System.out.println("Input: " + Arrays.toString(input5));
      Puzzle puzzle5 = new Puzzle(input5);
      System.out.println();
   }

   public int getResultCount() {
      return resultCount;
   }

   public List<List<Long>> getResults() {
      return results;
   }

   /**
    * Extracts necessary data for finding solution
    */
   private void extractData() {
      cantBeZero = Arrays.stream(words)
              .map(x -> x.charAt(0))
              .collect(Collectors.toSet());

      for (String word : words) {
         for (int i = 0; i < word.length(); i++) {
            char key = word.toUpperCase().charAt(i);

            if (!charIntMap.containsKey(key)) {
               charIntMap.put(key, -1);
               unique.append(key);
            }
         }
      }
   }

   /**
    * Convert string to long by using charIntMap values.
    * @param word - string to be converted
    * @return - converted string to long
    */
   public long getNumber(String word){
      StringBuilder number = new StringBuilder();

      for(int i = 0; i < word.length(); i++){
         char key = word.charAt(i);

         number.append(charIntMap.get(key));
      }

      return Long.parseLong(number.toString());
   }

   /**
    * Solve the word puzzle.
    */
   public void solve(int idx) {
      int maxWordLength = Math.max(words[0] == null ? 0 : words[0].length(), words[1] == null ? 0 : words[1].length());

      if (maxWordLength > words[2].length() || maxWordLength + 1 < words[2].length() || unique.length() > 10) {
         return;
      }

      if(idx == unique.length()){
         List<Long> values = new ArrayList<>();

         for (String word : words) {
            values.add(getNumber(word));
         }

         if(values.get(0) + values.get(1) == values.get(2)) {
            resultCount++;
            results.add(values);
         }

         return;
      }

      char key = unique.charAt(idx);

      for(int value = 0; value <= 9; value++){

         if(!usedNumbers[value]){
            if (value == 0 && cantBeZero.contains(key)) {
               continue;
            }

            usedNumbers[value] = true;
            charIntMap.put(key, value);

            solve(idx + 1);

            charIntMap.put(key, -1);
            usedNumbers[value] = false;
         }
      }
   }
}
